param(
  [Parameter(Mandatory=$false)]
  $path = $PSScriptroot,
  [Parameter(Mandatory=$false)]
  $extention = 'txt'
)

function Set-Randomizer {

  Push-Location $path

  $files = (Get-Childitem (Get-Location) -File).Name -like "*.$extention"
  $Length = $files.Length
  $form = ("$Length").Length

  $rand = Get-Random -Count $Length -InputObject (1..$Length) |
  Foreach-Object {
    if (("$_").Length -lt $form) {
      Foreach ($i in ($form - ("$_").Length)) {
        ("$((-join @(0) * ($i)))$_")
      }
    }
    else {
      $_
    }
  }

  $count = 0

  $files | Foreach-Object {

    $reg = [Regex]::new('[0-9]+\-').Matches($_).value

    if ([string]::IsNullOrEmpty($reg)) {
      Rename-Item -Path (Get-Childitem $_).FullName -NewName ("$($rand[$count])-$_") -Force
    }
    else {
      Rename-Item -Path (Get-Childitem $_).FullName -NewName ("$($rand[$count])-$($_ -replace "$($_.Split('-')[0])-",$null)") -Force
      # Rename-Item -Path (Get-Childitem $_).FullName -NewName ("$($rand[$count])-$($_.Split('-')[-1])") -Force
    }

      $count += 1
  }

  Write-Output "$Length fies has been mixed!"

  Pop-Location
}

Set-Randomizer $path $extention
