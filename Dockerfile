FROM jenkins/jenkins:lts
COPY --from=lachlanevenson/k8s-kubectl:v1.10.3 /usr/local/bin/kubectl /usr/local/bin/kubectl
USER root
RUN apt-get update && apt-get install -y \
    wget \
    unzip
RUN wget https://releases.hashicorp.com/terraform/0.12.13/terraform_0.12.13_linux_amd64.zip \
  && unzip terraform_0.12.13_linux_amd64.zip \
  && mv terraform /usr/local/bin/ \
  && rm terraform_0.12.13_linux_amd64.zip
USER jenkins